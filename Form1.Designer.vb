﻿Imports System

Namespace pos_fiscal_CSharp
    Partial Class Form1
        ''' <summary>
        ''' Variable del diseñador necesaria.
        ''' </summary>
        Private components As ComponentModel.IContainer = Nothing


        ''' <summary>
        ''' Limpiar los recursos que se estén usando.
        ''' </summary>
        ''' <paramname="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If

            MyBase.Dispose(disposing)
        End Sub


#Region "Código generado por el Diseñador de Windows Forms"

        ''' <summary>
        ''' Método necesario para admitir el Diseñador. No se puede modificar
        ''' el contenido de este método con el editor de código.
        ''' </summary>
        Private Sub InitializeComponent()
            ProgressBar1 = New Windows.Forms.ProgressBar()
            GroupBox4 = New Windows.Forms.GroupBox()
            btn_por_fecha = New Windows.Forms.Button()
            btn_por_numero = New Windows.Forms.Button()
            DateTimePicker2 = New Windows.Forms.DateTimePicker()
            DateTimePicker1 = New Windows.Forms.DateTimePicker()
            txt_z_1 = New Windows.Forms.TextBox()
            txt_z_2 = New Windows.Forms.TextBox()
            GroupBox3 = New Windows.Forms.GroupBox()
            btn_cierre_z = New Windows.Forms.Button()
            btn_informe_x = New Windows.Forms.Button()
            GroupBox2 = New Windows.Forms.GroupBox()
            btn_nc_exoneracion = New Windows.Forms.Button()
            btn_nc_credito_fiscal = New Windows.Forms.Button()
            btn_nc_consumidor_final = New Windows.Forms.Button()
            GroupBox1 = New Windows.Forms.GroupBox()
            btn_fact_con_exoneracion_de_impuestos = New Windows.Forms.Button()
            btn_fact_credito_fiscal = New Windows.Forms.Button()
            cmd_fact_consumidor_final = New Windows.Forms.Button()
            btn_no_venta = New Windows.Forms.Button()
            btn_libro_de_venta = New Windows.Forms.Button()
            btn_reconectar = New Windows.Forms.Button()
            GroupBox4.SuspendLayout()
            GroupBox3.SuspendLayout()
            GroupBox2.SuspendLayout()
            GroupBox1.SuspendLayout()
            SuspendLayout()
            ' 
            ' ProgressBar1
            ' 
            ProgressBar1.Dock = Windows.Forms.DockStyle.Bottom
            ProgressBar1.Location = New Drawing.Point(0, 544)
            ProgressBar1.MarqueeAnimationSpeed = 25
            ProgressBar1.Name = "ProgressBar1"
            ProgressBar1.Size = New Drawing.Size(392, 15)
            ProgressBar1.Style = Windows.Forms.ProgressBarStyle.Marquee
            ProgressBar1.TabIndex = 15
            ' 
            ' GroupBox4
            ' 
            GroupBox4.Anchor = Windows.Forms.AnchorStyles.Top Or Windows.Forms.AnchorStyles.Left Or Windows.Forms.AnchorStyles.Right
            GroupBox4.Controls.Add(btn_por_fecha)
            GroupBox4.Controls.Add(btn_por_numero)
            GroupBox4.Controls.Add(DateTimePicker2)
            GroupBox4.Controls.Add(DateTimePicker1)
            GroupBox4.Controls.Add(txt_z_1)
            GroupBox4.Controls.Add(txt_z_2)
            GroupBox4.Font = New Drawing.Font("Microsoft Sans Serif", 8.25F, Drawing.FontStyle.Bold, Drawing.GraphicsUnit.Point, 0)
            GroupBox4.Location = New Drawing.Point(12, 441)
            GroupBox4.Name = "GroupBox4"
            GroupBox4.Size = New Drawing.Size(368, 89)
            GroupBox4.TabIndex = 14
            GroupBox4.TabStop = False
            GroupBox4.Text = "Auditoria"
            ' 
            ' btn_por_fecha
            ' 
            btn_por_fecha.Anchor = Windows.Forms.AnchorStyles.Top Or Windows.Forms.AnchorStyles.Left Or Windows.Forms.AnchorStyles.Right
            btn_por_fecha.Font = New Drawing.Font("Microsoft Sans Serif", 8.25F, Drawing.FontStyle.Bold)
            btn_por_fecha.Location = New Drawing.Point(257, 53)
            btn_por_fecha.Name = "btn_por_fecha"
            btn_por_fecha.Size = New Drawing.Size(105, 23)
            btn_por_fecha.TabIndex = 8
            btn_por_fecha.Text = "Por fecha"
            btn_por_fecha.UseVisualStyleBackColor = True
            AddHandler btn_por_fecha.Click, New EventHandler(AddressOf btn_por_fecha_Click)
            ' 
            ' btn_por_numero
            ' 
            btn_por_numero.Anchor = Windows.Forms.AnchorStyles.Top Or Windows.Forms.AnchorStyles.Left Or Windows.Forms.AnchorStyles.Right
            btn_por_numero.Font = New Drawing.Font("Microsoft Sans Serif", 8.25F, Drawing.FontStyle.Bold)
            btn_por_numero.Location = New Drawing.Point(257, 28)
            btn_por_numero.Name = "btn_por_numero"
            btn_por_numero.Size = New Drawing.Size(105, 23)
            btn_por_numero.TabIndex = 7
            btn_por_numero.Text = "Por Número"
            btn_por_numero.UseVisualStyleBackColor = True
            AddHandler btn_por_numero.Click, New EventHandler(AddressOf btn_por_numero_Click)
            ' 
            ' DateTimePicker2
            ' 
            DateTimePicker2.Format = Windows.Forms.DateTimePickerFormat.Short
            DateTimePicker2.Location = New Drawing.Point(130, 55)
            DateTimePicker2.Name = "DateTimePicker2"
            DateTimePicker2.Size = New Drawing.Size(117, 20)
            DateTimePicker2.TabIndex = 6
            ' 
            ' DateTimePicker1
            ' 
            DateTimePicker1.Format = Windows.Forms.DateTimePickerFormat.Short
            DateTimePicker1.Location = New Drawing.Point(6, 55)
            DateTimePicker1.Name = "DateTimePicker1"
            DateTimePicker1.Size = New Drawing.Size(117, 20)
            DateTimePicker1.TabIndex = 5
            ' 
            ' txt_z_1
            ' 
            txt_z_1.Location = New Drawing.Point(6, 29)
            txt_z_1.Name = "txt_z_1"
            txt_z_1.Size = New Drawing.Size(117, 20)
            txt_z_1.TabIndex = 4
            ' 
            ' txt_z_2
            ' 
            txt_z_2.Location = New Drawing.Point(130, 29)
            txt_z_2.Name = "txt_z_2"
            txt_z_2.Size = New Drawing.Size(117, 20)
            txt_z_2.TabIndex = 2
            ' 
            ' GroupBox3
            ' 
            GroupBox3.Anchor = Windows.Forms.AnchorStyles.Top Or Windows.Forms.AnchorStyles.Left Or Windows.Forms.AnchorStyles.Right
            GroupBox3.Controls.Add(btn_cierre_z)
            GroupBox3.Controls.Add(btn_informe_x)
            GroupBox3.Font = New Drawing.Font("Microsoft Sans Serif", 8.25F, Drawing.FontStyle.Bold, Drawing.GraphicsUnit.Point, 0)
            GroupBox3.Location = New Drawing.Point(12, 330)
            GroupBox3.Name = "GroupBox3"
            GroupBox3.Size = New Drawing.Size(368, 105)
            GroupBox3.TabIndex = 13
            GroupBox3.TabStop = False
            GroupBox3.Text = "Reportes"
            ' 
            ' btn_cierre_z
            ' 
            btn_cierre_z.Anchor = Windows.Forms.AnchorStyles.Top Or Windows.Forms.AnchorStyles.Left Or Windows.Forms.AnchorStyles.Right
            btn_cierre_z.Font = New Drawing.Font("Arial", 11.25F, Drawing.FontStyle.Italic)
            btn_cierre_z.Location = New Drawing.Point(6, 60)
            btn_cierre_z.Name = "btn_cierre_z"
            btn_cierre_z.Size = New Drawing.Size(356, 29)
            btn_cierre_z.TabIndex = 1
            btn_cierre_z.Text = "Cierre Z"
            btn_cierre_z.UseVisualStyleBackColor = True
            AddHandler btn_cierre_z.Click, New EventHandler(AddressOf btn_cierre_z_Click)
            ' 
            ' btn_informe_x
            ' 
            btn_informe_x.Anchor = Windows.Forms.AnchorStyles.Top Or Windows.Forms.AnchorStyles.Left Or Windows.Forms.AnchorStyles.Right
            btn_informe_x.Font = New Drawing.Font("Arial", 11.25F, Drawing.FontStyle.Italic)
            btn_informe_x.Location = New Drawing.Point(6, 25)
            btn_informe_x.Name = "btn_informe_x"
            btn_informe_x.Size = New Drawing.Size(356, 29)
            btn_informe_x.TabIndex = 0
            btn_informe_x.Text = "Informe X"
            btn_informe_x.UseVisualStyleBackColor = True
            AddHandler btn_informe_x.Click, New EventHandler(AddressOf btn_informe_x_Click)
            ' 
            ' GroupBox2
            ' 
            GroupBox2.Anchor = Windows.Forms.AnchorStyles.Top Or Windows.Forms.AnchorStyles.Left Or Windows.Forms.AnchorStyles.Right
            GroupBox2.Controls.Add(btn_nc_exoneracion)
            GroupBox2.Controls.Add(btn_nc_credito_fiscal)
            GroupBox2.Controls.Add(btn_nc_consumidor_final)
            GroupBox2.Font = New Drawing.Font("Microsoft Sans Serif", 8.25F, Drawing.FontStyle.Bold, Drawing.GraphicsUnit.Point, 0)
            GroupBox2.Location = New Drawing.Point(12, 186)
            GroupBox2.Name = "GroupBox2"
            GroupBox2.Size = New Drawing.Size(368, 138)
            GroupBox2.TabIndex = 12
            GroupBox2.TabStop = False
            GroupBox2.Text = "Notas de crédito"
            ' 
            ' btn_nc_exoneracion
            ' 
            btn_nc_exoneracion.Anchor = Windows.Forms.AnchorStyles.Top Or Windows.Forms.AnchorStyles.Left Or Windows.Forms.AnchorStyles.Right
            btn_nc_exoneracion.Font = New Drawing.Font("Arial", 11.25F, Drawing.FontStyle.Italic)
            btn_nc_exoneracion.Location = New Drawing.Point(6, 95)
            btn_nc_exoneracion.Name = "btn_nc_exoneracion"
            btn_nc_exoneracion.Size = New Drawing.Size(356, 29)
            btn_nc_exoneracion.TabIndex = 2
            btn_nc_exoneracion.Text = "Nota de crédito con exoneración"
            btn_nc_exoneracion.UseVisualStyleBackColor = True
            AddHandler btn_nc_exoneracion.Click, New EventHandler(AddressOf btn_nc_exoneracion_Click)
            ' 
            ' btn_nc_credito_fiscal
            ' 
            btn_nc_credito_fiscal.Anchor = Windows.Forms.AnchorStyles.Top Or Windows.Forms.AnchorStyles.Left Or Windows.Forms.AnchorStyles.Right
            btn_nc_credito_fiscal.Font = New Drawing.Font("Arial", 11.25F, Drawing.FontStyle.Italic)
            btn_nc_credito_fiscal.Location = New Drawing.Point(6, 60)
            btn_nc_credito_fiscal.Name = "btn_nc_credito_fiscal"
            btn_nc_credito_fiscal.Size = New Drawing.Size(356, 29)
            btn_nc_credito_fiscal.TabIndex = 1
            btn_nc_credito_fiscal.Text = "Nota de crédito a crédito fiscal"
            btn_nc_credito_fiscal.UseVisualStyleBackColor = True
            AddHandler btn_nc_credito_fiscal.Click, New EventHandler(AddressOf btn_nc_credito_fiscal_Click)
            ' 
            ' btn_nc_consumidor_final
            ' 
            btn_nc_consumidor_final.Anchor = Windows.Forms.AnchorStyles.Top Or Windows.Forms.AnchorStyles.Left Or Windows.Forms.AnchorStyles.Right
            btn_nc_consumidor_final.Font = New Drawing.Font("Arial", 11.25F, Drawing.FontStyle.Italic)
            btn_nc_consumidor_final.Location = New Drawing.Point(6, 25)
            btn_nc_consumidor_final.Name = "btn_nc_consumidor_final"
            btn_nc_consumidor_final.Size = New Drawing.Size(356, 29)
            btn_nc_consumidor_final.TabIndex = 0
            btn_nc_consumidor_final.Text = "Nota de crédito a consumidor final"
            btn_nc_consumidor_final.UseVisualStyleBackColor = True
            AddHandler btn_nc_consumidor_final.Click, New EventHandler(AddressOf btn_nc_consumidor_final_Click)
            ' 
            ' GroupBox1
            ' 
            GroupBox1.Anchor = Windows.Forms.AnchorStyles.Top Or Windows.Forms.AnchorStyles.Left Or Windows.Forms.AnchorStyles.Right
            GroupBox1.Controls.Add(btn_fact_con_exoneracion_de_impuestos)
            GroupBox1.Controls.Add(btn_fact_credito_fiscal)
            GroupBox1.Controls.Add(cmd_fact_consumidor_final)
            GroupBox1.Font = New Drawing.Font("Microsoft Sans Serif", 8.25F, Drawing.FontStyle.Bold, Drawing.GraphicsUnit.Point, 0)
            GroupBox1.Location = New Drawing.Point(12, 42)
            GroupBox1.Name = "GroupBox1"
            GroupBox1.Size = New Drawing.Size(368, 138)
            GroupBox1.TabIndex = 11
            GroupBox1.TabStop = False
            GroupBox1.Text = "Facturación"
            ' 
            ' btn_fact_con_exoneracion_de_impuestos
            ' 
            btn_fact_con_exoneracion_de_impuestos.Anchor = Windows.Forms.AnchorStyles.Top Or Windows.Forms.AnchorStyles.Left Or Windows.Forms.AnchorStyles.Right
            btn_fact_con_exoneracion_de_impuestos.Font = New Drawing.Font("Arial", 11.25F, Drawing.FontStyle.Italic, Drawing.GraphicsUnit.Point, 0)
            btn_fact_con_exoneracion_de_impuestos.Location = New Drawing.Point(6, 95)
            btn_fact_con_exoneracion_de_impuestos.Name = "btn_fact_con_exoneracion_de_impuestos"
            btn_fact_con_exoneracion_de_impuestos.Size = New Drawing.Size(356, 29)
            btn_fact_con_exoneracion_de_impuestos.TabIndex = 2
            btn_fact_con_exoneracion_de_impuestos.Text = "Factura con exoneración de impuestos"
            btn_fact_con_exoneracion_de_impuestos.UseVisualStyleBackColor = True
            AddHandler btn_fact_con_exoneracion_de_impuestos.Click, New EventHandler(AddressOf btn_fact_con_exoneracion_de_impuestos_Click)
            ' 
            ' btn_fact_credito_fiscal
            ' 
            btn_fact_credito_fiscal.Anchor = Windows.Forms.AnchorStyles.Top Or Windows.Forms.AnchorStyles.Left Or Windows.Forms.AnchorStyles.Right
            btn_fact_credito_fiscal.Font = New Drawing.Font("Arial", 11.25F, Drawing.FontStyle.Italic, Drawing.GraphicsUnit.Point, 0)
            btn_fact_credito_fiscal.Location = New Drawing.Point(6, 60)
            btn_fact_credito_fiscal.Name = "btn_fact_credito_fiscal"
            btn_fact_credito_fiscal.Size = New Drawing.Size(356, 29)
            btn_fact_credito_fiscal.TabIndex = 1
            btn_fact_credito_fiscal.Text = "Factura crédito fiscal"
            btn_fact_credito_fiscal.UseVisualStyleBackColor = True
            AddHandler btn_fact_credito_fiscal.Click, New EventHandler(AddressOf btn_fact_credito_fiscal_Click)
            ' 
            ' cmd_fact_consumidor_final
            ' 
            cmd_fact_consumidor_final.Anchor = Windows.Forms.AnchorStyles.Top Or Windows.Forms.AnchorStyles.Left Or Windows.Forms.AnchorStyles.Right
            cmd_fact_consumidor_final.Font = New Drawing.Font("Arial", 11.25F, Drawing.FontStyle.Italic, Drawing.GraphicsUnit.Point, 0)
            cmd_fact_consumidor_final.Location = New Drawing.Point(6, 25)
            cmd_fact_consumidor_final.Name = "cmd_fact_consumidor_final"
            cmd_fact_consumidor_final.Size = New Drawing.Size(356, 29)
            cmd_fact_consumidor_final.TabIndex = 0
            cmd_fact_consumidor_final.Text = "Factura consumidor final"
            cmd_fact_consumidor_final.UseVisualStyleBackColor = True
            AddHandler cmd_fact_consumidor_final.Click, New EventHandler(AddressOf cmd_fact_consumidor_final_Click)
            ' 
            ' btn_no_venta
            ' 
            btn_no_venta.Font = New Drawing.Font("Microsoft Sans Serif", 8.25F, Drawing.FontStyle.Bold, Drawing.GraphicsUnit.Point, 0)
            btn_no_venta.Location = New Drawing.Point(105, 12)
            btn_no_venta.Name = "btn_no_venta"
            btn_no_venta.Size = New Drawing.Size(87, 23)
            btn_no_venta.TabIndex = 10
            btn_no_venta.Text = "No Venta"
            btn_no_venta.UseVisualStyleBackColor = True
            AddHandler btn_no_venta.Click, New EventHandler(AddressOf btn_no_venta_Click)
            ' 
            ' btn_libro_de_venta
            ' 
            btn_libro_de_venta.Anchor = Windows.Forms.AnchorStyles.Top Or Windows.Forms.AnchorStyles.Left Or Windows.Forms.AnchorStyles.Right
            btn_libro_de_venta.Font = New Drawing.Font("Microsoft Sans Serif", 8.25F, Drawing.FontStyle.Bold, Drawing.GraphicsUnit.Point, 0)
            btn_libro_de_venta.Location = New Drawing.Point(198, 12)
            btn_libro_de_venta.Name = "btn_libro_de_venta"
            btn_libro_de_venta.Size = New Drawing.Size(182, 23)
            btn_libro_de_venta.TabIndex = 9
            btn_libro_de_venta.Text = "Libro de venta"
            btn_libro_de_venta.UseVisualStyleBackColor = True
            AddHandler btn_libro_de_venta.Click, New EventHandler(AddressOf btn_libro_de_venta_Click)
            ' 
            ' btn_reconectar
            ' 
            btn_reconectar.Font = New Drawing.Font("Microsoft Sans Serif", 8.25F, Drawing.FontStyle.Bold, Drawing.GraphicsUnit.Point, 0)
            btn_reconectar.Location = New Drawing.Point(12, 12)
            btn_reconectar.Name = "btn_reconectar"
            btn_reconectar.Size = New Drawing.Size(87, 23)
            btn_reconectar.TabIndex = 8
            btn_reconectar.Text = "Reconectar"
            btn_reconectar.UseVisualStyleBackColor = True
            AddHandler btn_reconectar.Click, New EventHandler(AddressOf btn_reconectar_Click)
            ' 
            ' Form1
            ' 
            AutoScaleDimensions = New Drawing.SizeF(6.0F, 13.0F)
            AutoScaleMode = Windows.Forms.AutoScaleMode.Font
            ClientSize = New Drawing.Size(392, 559)
            Controls.Add(ProgressBar1)
            Controls.Add(GroupBox4)
            Controls.Add(GroupBox3)
            Controls.Add(GroupBox2)
            Controls.Add(GroupBox1)
            Controls.Add(btn_no_venta)
            Controls.Add(btn_libro_de_venta)
            Controls.Add(btn_reconectar)
            MaximizeBox = False
            MinimizeBox = False
            Name = "Form1"
            StartPosition = Windows.Forms.FormStartPosition.CenterScreen
            Text = "POS Fiscal"
            AddHandler Load, New EventHandler(AddressOf Form1_Load)
            GroupBox4.ResumeLayout(False)
            GroupBox4.PerformLayout()
            GroupBox3.ResumeLayout(False)
            GroupBox2.ResumeLayout(False)
            GroupBox1.ResumeLayout(False)
            ResumeLayout(False)
        End Sub


#End Region

        Friend ProgressBar1 As Windows.Forms.ProgressBar
        Friend GroupBox4 As Windows.Forms.GroupBox
        Friend btn_por_fecha As Windows.Forms.Button
        Friend btn_por_numero As Windows.Forms.Button
        Friend DateTimePicker2 As Windows.Forms.DateTimePicker
        Friend DateTimePicker1 As Windows.Forms.DateTimePicker
        Friend txt_z_1 As Windows.Forms.TextBox
        Friend txt_z_2 As Windows.Forms.TextBox
        Friend GroupBox3 As Windows.Forms.GroupBox
        Friend btn_cierre_z As Windows.Forms.Button
        Friend btn_informe_x As Windows.Forms.Button
        Friend GroupBox2 As Windows.Forms.GroupBox
        Friend btn_nc_exoneracion As Windows.Forms.Button
        Friend btn_nc_credito_fiscal As Windows.Forms.Button
        Friend btn_nc_consumidor_final As Windows.Forms.Button
        Friend GroupBox1 As Windows.Forms.GroupBox
        Friend btn_fact_con_exoneracion_de_impuestos As Windows.Forms.Button
        Friend btn_fact_credito_fiscal As Windows.Forms.Button
        Friend cmd_fact_consumidor_final As Windows.Forms.Button
        Friend btn_no_venta As Windows.Forms.Button
        Friend btn_libro_de_venta As Windows.Forms.Button
        Friend btn_reconectar As Windows.Forms.Button
    End Class
End Namespace
