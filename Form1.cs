﻿using AxEpsonFPHostControlX;
using Epson_Custom;
using MetodosDeExtension;
using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Windows.Forms;

namespace pos_fiscal_CSharp
{
    public partial class Form1 : Form
    {

        AxEpsonFPHostControl Impresora = null;
        string error = "0";

        public Form1()
        {
            InitializeComponent();
            Load += Form1_Load;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //ConectarConImpresora();
            ProgressBar1.Visible = false;

        }

        private void ConectarConImpresora()
        {
            ProgressBar1.Visible = true;
            this.Enabled = false;

            Impresora = CrearImpresoraFiscal.Impresora();
            if (Impresora == null)
            {
                Impresora = CrearImpresoraFiscal2.Impresora();
                if (Impresora == null)
                {
                    Impresora = CrearImpresoraFiscal3.Impresora();
                    if (Impresora == null)
                    {
                        Impresora = CrearImpresoraFiscal4.Impresora();
                        if (Impresora == null)
                        {
                            Mensaje("No se pudo conectar a la impresora");
                        }
                    }
                }
            }

            if (Impresora != null)
            {
                btn_reconectar.Enabled = false;

                Comandos_de_comprobante_fiscal.cancelarComprobante(Impresora);
            }

            ProgressBar1.Visible = false;
            this.Enabled = true;
        }

        private void Mensaje(string error)
        {
            MessageBox.Show(error, Text);
            this.EscribirEnArchivoLog(error);
        }

        private void btn_reconectar_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Desea Reconectar con la impresora?", Text, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                ConectarConImpresora();
            }
        }

        private void btn_no_venta_Click(object sender, EventArgs e)
        {
            if(Impresora == null)
            {
                Mensaje("Impresora fuera de linea");
                return;
            }

            Comandosdedocumentonofiscal.Abrirdocumentonofiscal(Impresora);
            for(int i = 0; i < 10; i++)
            {
                error = Comandosdedocumentonofiscal.Imprimirlineaendocumentonofiscal(Impresora, "Linea " + i);

                if(error != "0")
                {
                    Mensaje(error);
                }
            }

            error = Comandosdedocumentonofiscal.Informaciondedocumentonofiscal(Impresora);

            if(error != "0") Mensaje(error);

            Mensaje(string.Format("Lineas impresas {0}, Id {1}",
                Comandosdedocumentonofiscal.CantidadDelineasimpresas,
                Comandosdedocumentonofiscal.Numerodedocumentonofiscal));

            error = Comandosdedocumentonofiscal.Cerrardocumentonofiscal(Impresora);
            if(error != "0") Mensaje(error);
            
        }

        /// <summary>
        /// 1 - Factura a consumidor final.
        /// 2 - Factura con derecho a credito fiscal
        /// 3 - Nota de credito a consumidor final
        /// 4 - Nota de credito con derecho a crdito fiscal
        /// 5 - Factura a consumidor final con exoneracion de ITBIS
        /// 6 - Factura con derecho a credito fiscal con exoneracion de ITBIS
        /// 7 - Nota de credito a consumidor final con exoneracion de ITBIS
        /// 8 - Nota de credito con derecho a crdito fiscal con exoneracion de ITBIS
        /// </summary>
        private void cmd_fact_consumidor_final_Click(object sender, EventArgs e)
        {
            if (Impresora == null)
            {
                Mensaje("Impresora fuera de línea");
                return;
            }

            error = Comandos_de_comprobante_fiscal.AbrirDocumentoFiscal(Impresora, 1, 0, false, "", "", "0001", "0002", "", "", "", "");
            if (error != "0")
            {
                Mensaje(error);
                return;
            }

            ProcesoFacturacion();
        }

        private void ProcesoFacturacion()
        {
            //enviamos los items
            for(int i = 0; i < 10; i++)
            {
                error = Comandos_de_comprobante_fiscal.EnviarItem(Impresora, "Descripción " + i, "1000", "1000", "1800");
                if (error != "0")
                {
                    Mensaje(error);
                    return;
                }
            }

            //Descuento por items
            for (int i = 0; i < 3; i++)
            {
                //las primeras 9 descripciones son opcionales, la decima es la descripcion del item
                error = Comandos_de_comprobante_fiscal.Descuentoporitem(Impresora, 
                    "","","","","","","","","", "Descuento descripción " + i, "1000", "1000", "1800");
                if (error != "0")
                { 
                    Mensaje(error);
                    return;
                }
            }

            //Recargo por items
            for (int i = 0; i < 3; i++)
            {
                //las primeras 9 descripciones son opcionales, la decima es la descripcion del item
                error = Comandos_de_comprobante_fiscal.Recargoporitem(Impresora,
                    "", "", "", "", "", "", "", "", "", "Recargo descripción " + i, "1000", "1000", "1800");
                if (error != "0")
                {
                    Mensaje(error);
                    return;
                }
            }

            //solicitamos el subtotal
             error = Comandos_de_comprobante_fiscal.ObtenerSubTotal(Impresora);
            if (error != "0")
            {
                Mensaje(error);
                return;
            }

            //solicitamos descuento a sub-total
            error = Comandos_de_comprobante_fiscal.Descuento(Impresora, "Descuento", "1000");
            if (error != "0")
            {
                Mensaje(error);
                return;
            }

            //solicitamos descuento a sub-total
            error = Comandos_de_comprobante_fiscal.Recargos(Impresora, "Recargos", "1000");
            if (error != "0")
            {
                Mensaje(error);
                return;
            }

            //procedemos a aplicar el pago total
            Comandos_de_comprobante_fiscal.Pagar(Impresora, "001", "100000", "", "", "");

            //Enviamos algunos comentarios de hasta 40 lineas
            error = Comandos_de_comprobante_fiscal.ImprimirLineaEnComprobante(Impresora, "Comentario de prueba");
            if (error != "0") Mensaje(error);

            //Procedemos a cerrar el comprobante
            error = Comandos_de_comprobante_fiscal.CerrarComprobante(Impresora);
            if (error != "0") Mensaje(error);
        }

        private void btn_fact_credito_fiscal_Click(object sender, EventArgs e)
        {
            if (Impresora == null)
            {
                Mensaje("Impresora fuera de línea");
                return;
            }

            error = Comandos_de_comprobante_fiscal.AbrirDocumentoFiscal(Impresora, 2, 0, false, "", "", "0001", "0002","A1234567890123456789", 
                "Nombre del cliente", "123456789", "");
            if (error != "0")
            {
                Mensaje(error);
                return;
            }

            ProcesoFacturacion();
        }

        private void btn_fact_con_exoneracion_de_impuestos_Click(object sender, EventArgs e)
        {
            if (Impresora == null)
            {
                Mensaje("Impresora fuera de línea");
                return;
            }

            error = Comandos_de_comprobante_fiscal.AbrirDocumentoFiscal(Impresora, 6, 0, false, "", "", "0001", "0002", "A1234567890123456789",
                "Nombre del cliente", "123456789", "");
            if (error != "0")
            {
                Mensaje(error);
                return;
            }

            ProcesoFacturacion();
        }

        private void btn_nc_consumidor_final_Click(object sender, EventArgs e)
        {
            if (Impresora == null)
            {
                Mensaje("Impresora fuera de línea");
                return;
            }

            error = Comandos_de_comprobante_fiscal.AbrirDocumentoFiscal(Impresora, 3, 0, false, "", "", "0001", "0002", "","", "", "");
            if (error != "0")
            {
                Mensaje(error);
                return;
            }

            ProcesoFacturacion();
        }

        private void btn_nc_credito_fiscal_Click(object sender, EventArgs e)
        {
            if (Impresora == null)
            {
                Mensaje("Impresora fuera de línea");
                return;
            }

            error = Comandos_de_comprobante_fiscal.AbrirDocumentoFiscal(Impresora, 4, 0, false, "", "", "0001", "0002", 
                "A1234567890123456789", "Cliente", "123456789", "A1234567890123456788");
            if (error != "0")
            {
                Mensaje(error);
                return;
            }

            ProcesoFacturacion();
        }

        private void btn_nc_exoneracion_Click(object sender, EventArgs e)
        {
            if (Impresora == null)
            {
                Mensaje("Impresora fuera de línea");
                return;
            }

            error = Comandos_de_comprobante_fiscal.AbrirDocumentoFiscal(Impresora, 8, 0, false, "", "", "0001", "0002",
                "A1234567890123456789", "Cliente", "123456789", "A1234567890123456788");
            if (error != "0")
            {
                Mensaje(error);
                return;
            }

            ProcesoFacturacion(); 
        }

        private void btn_informe_x_Click(object sender, EventArgs e)
        {
            if (Impresora == null)
            {
                Mensaje("Impresora fuera de línea");
                return;
            }

            ProgressBar1.Visible = true;
            this.Enabled = false;
            ComandosDeJornadaFiscal.InformeX(Impresora);

            ProgressBar1.Visible = false;
            this.Enabled = true;
        }

        private void btn_cierre_z_Click(object sender, EventArgs e)
        {
            if (Impresora == null)
            {
                Mensaje("Impresora fuera de línea");
                return;
            }

            ProgressBar1.Visible = true;
            this.Enabled = false;

            if(MessageBox.Show("Deseas imprimir el cierre", Text, MessageBoxButtons.YesNo) == DialogResult.Yes)
                ComandosDeJornadaFiscal.CierreZImpreso(Impresora);
            else
                ComandosDeJornadaFiscal.CierreZSinImprimir(Impresora);

            Mensaje("Número de cierre generado " + ComandosDeJornadaFiscal.NumeroDeCierreZ);

            ProgressBar1.Visible = false;
            this.Enabled = true;
        }

        private void btn_por_numero_Click(object sender, EventArgs e)
        {
            if (Impresora == null)
            {
                Mensaje("Impresora fuera de línea");
                return;
            }

            ProgressBar1.Visible = true;
            this.Enabled = false; //se desabilita el formulario

            int cierre1, cierre2;
            if(!int.TryParse(txt_z_1.Text, out cierre1))
            {
                Mensaje(string.Format("Error al convertir '{0}' en número", txt_z_1.Text));
                ProgressBar1.Visible = false;
                this.Enabled = true;
                return;
            }
            if (!int.TryParse(txt_z_2.Text, out cierre2))
            {
                Mensaje(string.Format("Error al convertir '{0}' en número", txt_z_2.Text));
                ProgressBar1.Visible = false;
                this.Enabled = true;
                return;
            }

            //Iniciamos el reporte
            Comandosdeauditoria.IniciarinformedecierresZporrangodecierresZ_Impreso(Impresora, cierre1.ToString(), cierre2.ToString());

            //Pedimos los datos
            Comandosdeauditoria.ObtenersiguientesdatosdeinformedecierresZ(Impresora);

            while (Comandosdeauditoria.Indicadordedatospordescargar == "S")
            {
                //Pedimos los datos mientras hayan disponibles
                Comandosdeauditoria.ObtenersiguientesdatosdeinformedecierresZ(Impresora);
            }
            
            //Finalizaos el informe
            Comandosdeauditoria.FinalizarinformedecierresZ(Impresora);
            ProgressBar1.Visible = false;
            this.Enabled = true;
        }

        private void btn_por_fecha_Click(object sender, EventArgs e)
        {
            if (Impresora == null)
            {
                Mensaje("Impresora fuera de línea");
                return;
            }

            ProgressBar1.Visible = true;
            this.Enabled = false; //se desabilita el formulario
            DateTime fecha1, fecha2;
            if(!DateTime.TryParse(DateTimePicker1.Text, out fecha1))
            {
                Mensaje(string.Format("Error al convertir '{0}' en número", DateTimePicker1.Text));
            }
            if (!DateTime.TryParse(DateTimePicker2.Text, out fecha2))
            {
                Mensaje(string.Format("Error al convertir '{0}' en número", DateTimePicker2.Text));
            }

            //Iniciamos el reporte
            Comandosdeauditoria.IniciarInformeCierreZporFecha_Impreso(Impresora,
                fecha1.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture).Replace("/", ""),
                fecha1.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture).Replace("/", ""));

            //Pedimos los datos
            Comandosdeauditoria.ObtenersiguientesdatosdeinformedecierresZ(Impresora);

            while(Comandosdeauditoria.Indicadordedatospordescargar == "S")
            {
                //Pedimos los datos mientras hayan disponibles
                Comandosdeauditoria.ObtenersiguientesdatosdeinformedecierresZ(Impresora);
            }

            //Finalizamos el informe
            Comandosdeauditoria.FinalizarinformedecierresZ(Impresora);
            ProgressBar1.Visible = false;
            this.Enabled = true;
        }

        private void btn_libro_de_venta_Click(object sender, EventArgs e)
        {
            string ruta = Directory.GetCurrentDirectory() + "\\Libro\\testDllApp.exe";
            Process.Start(ruta);
        }
    }
}
