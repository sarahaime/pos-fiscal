﻿Imports AxEpsonFPHostControlX
Imports Epson_Custom
Imports MetodosDeExtension
Imports System
Imports System.Diagnostics
Imports System.Globalization
Imports System.IO
Imports System.Windows.Forms

Namespace pos_fiscal_CSharp
    Public Partial Class Form1
        Inherits Form

        Private Impresora As AxEpsonFPHostControl = Nothing
        Private [error] As String = "0"

        Public Sub New()
            InitializeComponent()
            AddHandler Load, AddressOf Form1_Load
        End Sub

        Private Sub Form1_Load(ByVal sender As Object, ByVal e As EventArgs)
            'ConectarConImpresora();
            ProgressBar1.Visible = False
        End Sub

        Private Sub ConectarConImpresora()
            ProgressBar1.Visible = True
            Enabled = False
            Impresora = CrearImpresoraFiscal.Impresora()

            If Impresora Is Nothing Then
                Impresora = CrearImpresoraFiscal2.Impresora()

                If Impresora Is Nothing Then
                    Impresora = CrearImpresoraFiscal3.Impresora()

                    If Impresora Is Nothing Then
                        Impresora = CrearImpresoraFiscal4.Impresora()

                        If Impresora Is Nothing Then
                            Mensaje("No se pudo conectar a la impresora")
                        End If
                    End If
                End If
            End If

            If Impresora IsNot Nothing Then
                btn_reconectar.Enabled = False
                Comandos_de_comprobante_fiscal.cancelarComprobante(Impresora)
            End If

            ProgressBar1.Visible = False
            Enabled = True
        End Sub

        Private Sub Mensaje(ByVal [error] As String)
            MessageBox.Show([error], Text)
            EscribirEnArchivoLog([error])
        End Sub

        Private Sub btn_reconectar_Click(ByVal sender As Object, ByVal e As EventArgs)
            If MessageBox.Show("Desea Reconectar con la impresora?", Text, MessageBoxButtons.YesNo) = DialogResult.Yes Then
                ConectarConImpresora()
            End If
        End Sub

        Private Sub btn_no_venta_Click(ByVal sender As Object, ByVal e As EventArgs)
            If Impresora Is Nothing Then
                Mensaje("Impresora fuera de linea")
                Return
            End If

            Comandosdedocumentonofiscal.Abrirdocumentonofiscal(Impresora)

            For i As Integer = 0 To 10 - 1
                [error] = Comandosdedocumentonofiscal.Imprimirlineaendocumentonofiscal(Impresora, "Linea " & i)

                If Not Equals([error], "0") Then
                    Mensaje([error])
                End If
            Next

            [error] = Comandosdedocumentonofiscal.Informaciondedocumentonofiscal(Impresora)
            If Not Equals([error], "0") Then Mensaje([error])
            Mensaje(String.Format("Lineas impresas {0}, Id {1}", Comandosdedocumentonofiscal.CantidadDelineasimpresas, Comandosdedocumentonofiscal.Numerodedocumentonofiscal))
            [error] = Comandosdedocumentonofiscal.Cerrardocumentonofiscal(Impresora)
            If Not Equals([error], "0") Then Mensaje([error])
        End Sub


        ''' <summary>
        ''' 1 - Factura a consumidor final.
        ''' 2 - Factura con derecho a credito fiscal
        ''' 3 - Nota de credito a consumidor final
        ''' 4 - Nota de credito con derecho a crdito fiscal
        ''' 5 - Factura a consumidor final con exoneracion de ITBIS
        ''' 6 - Factura con derecho a credito fiscal con exoneracion de ITBIS
        ''' 7 - Nota de credito a consumidor final con exoneracion de ITBIS
        ''' 8 - Nota de credito con derecho a crdito fiscal con exoneracion de ITBIS
        ''' </summary>
        Private Sub cmd_fact_consumidor_final_Click(ByVal sender As Object, ByVal e As EventArgs)
            If Impresora Is Nothing Then
                Mensaje("Impresora fuera de línea")
                Return
            End If

            [error] = Comandos_de_comprobante_fiscal.AbrirDocumentoFiscal(Impresora, 1, 0, False, "", "", "0001", "0002", "", "", "", "")

            If Not Equals([error], "0") Then
                Mensaje([error])
                Return
            End If

            ProcesoFacturacion()
        End Sub

        Private Sub ProcesoFacturacion()
            'enviamos los items
            For i As Integer = 0 To 10 - 1
                [error] = Comandos_de_comprobante_fiscal.EnviarItem(Impresora, "Descripción " & i, "1000", "1000", "1800")

                If Not Equals([error], "0") Then
                    Mensaje([error])
                    Return
                End If
            Next


            'Descuento por items
            For i As Integer = 0 To 3 - 1
                'las primeras 9 descripciones son opcionales, la decima es la descripcion del item
                [error] = Comandos_de_comprobante_fiscal.Descuentoporitem(Impresora, "", "", "", "", "", "", "", "", "", "Descuento descripción " & i, "1000", "1000", "1800")

                If Not Equals([error], "0") Then
                    Mensaje([error])
                    Return
                End If
            Next


            'Recargo por items
            For i As Integer = 0 To 3 - 1
                'las primeras 9 descripciones son opcionales, la decima es la descripcion del item
                [error] = Comandos_de_comprobante_fiscal.Recargoporitem(Impresora, "", "", "", "", "", "", "", "", "", "Recargo descripción " & i, "1000", "1000", "1800")

                If Not Equals([error], "0") Then
                    Mensaje([error])
                    Return
                End If
            Next


            'solicitamos el subtotal
            [error] = Comandos_de_comprobante_fiscal.ObtenerSubTotal(Impresora)

            If Not Equals([error], "0") Then
                Mensaje([error])
                Return
            End If


            'solicitamos descuento a sub-total
            [error] = Comandos_de_comprobante_fiscal.Descuento(Impresora, "Descuento", "1000")

            If Not Equals([error], "0") Then
                Mensaje([error])
                Return
            End If


            'solicitamos descuento a sub-total
            [error] = Comandos_de_comprobante_fiscal.Recargos(Impresora, "Recargos", "1000")

            If Not Equals([error], "0") Then
                Mensaje([error])
                Return
            End If


            'procedemos a aplicar el pago total
            Comandos_de_comprobante_fiscal.Pagar(Impresora, "001", "100000", "", "", "")

            'Enviamos algunos comentarios de hasta 40 lineas
            [error] = Comandos_de_comprobante_fiscal.ImprimirLineaEnComprobante(Impresora, "Comentario de prueba")
            If Not Equals([error], "0") Then Mensaje([error])

            'Procedemos a cerrar el comprobante
            [error] = Comandos_de_comprobante_fiscal.CerrarComprobante(Impresora)
            If Not Equals([error], "0") Then Mensaje([error])
        End Sub

        Private Sub btn_fact_credito_fiscal_Click(ByVal sender As Object, ByVal e As EventArgs)
            If Impresora Is Nothing Then
                Mensaje("Impresora fuera de línea")
                Return
            End If

            [error] = Comandos_de_comprobante_fiscal.AbrirDocumentoFiscal(Impresora, 2, 0, False, "", "", "0001", "0002", "A1234567890123456789", "Nombre del cliente", "123456789", "")

            If Not Equals([error], "0") Then
                Mensaje([error])
                Return
            End If

            ProcesoFacturacion()
        End Sub

        Private Sub btn_fact_con_exoneracion_de_impuestos_Click(ByVal sender As Object, ByVal e As EventArgs)
            If Impresora Is Nothing Then
                Mensaje("Impresora fuera de línea")
                Return
            End If

            [error] = Comandos_de_comprobante_fiscal.AbrirDocumentoFiscal(Impresora, 6, 0, False, "", "", "0001", "0002", "A1234567890123456789", "Nombre del cliente", "123456789", "")

            If Not Equals([error], "0") Then
                Mensaje([error])
                Return
            End If

            ProcesoFacturacion()
        End Sub

        Private Sub btn_nc_consumidor_final_Click(ByVal sender As Object, ByVal e As EventArgs)
            If Impresora Is Nothing Then
                Mensaje("Impresora fuera de línea")
                Return
            End If

            [error] = Comandos_de_comprobante_fiscal.AbrirDocumentoFiscal(Impresora, 3, 0, False, "", "", "0001", "0002", "", "", "", "")

            If Not Equals([error], "0") Then
                Mensaje([error])
                Return
            End If

            ProcesoFacturacion()
        End Sub

        Private Sub btn_nc_credito_fiscal_Click(ByVal sender As Object, ByVal e As EventArgs)
            If Impresora Is Nothing Then
                Mensaje("Impresora fuera de línea")
                Return
            End If

            [error] = Comandos_de_comprobante_fiscal.AbrirDocumentoFiscal(Impresora, 4, 0, False, "", "", "0001", "0002", "A1234567890123456789", "Cliente", "123456789", "A1234567890123456788")

            If Not Equals([error], "0") Then
                Mensaje([error])
                Return
            End If

            ProcesoFacturacion()
        End Sub

        Private Sub btn_nc_exoneracion_Click(ByVal sender As Object, ByVal e As EventArgs)
            If Impresora Is Nothing Then
                Mensaje("Impresora fuera de línea")
                Return
            End If

            [error] = Comandos_de_comprobante_fiscal.AbrirDocumentoFiscal(Impresora, 8, 0, False, "", "", "0001", "0002", "A1234567890123456789", "Cliente", "123456789", "A1234567890123456788")

            If Not Equals([error], "0") Then
                Mensaje([error])
                Return
            End If

            ProcesoFacturacion()
        End Sub

        Private Sub btn_informe_x_Click(ByVal sender As Object, ByVal e As EventArgs)
            If Impresora Is Nothing Then
                Mensaje("Impresora fuera de línea")
                Return
            End If

            ProgressBar1.Visible = True
            Enabled = False
            ComandosDeJornadaFiscal.InformeX(Impresora)
            ProgressBar1.Visible = False
            Enabled = True
        End Sub

        Private Sub btn_cierre_z_Click(ByVal sender As Object, ByVal e As EventArgs)
            If Impresora Is Nothing Then
                Mensaje("Impresora fuera de línea")
                Return
            End If

            ProgressBar1.Visible = True
            Enabled = False

            If MessageBox.Show("Deseas imprimir el cierre", Text, MessageBoxButtons.YesNo) = DialogResult.Yes Then
                ComandosDeJornadaFiscal.CierreZImpreso(Impresora)
            Else
                ComandosDeJornadaFiscal.CierreZSinImprimir(Impresora)
            End If

            Mensaje("Número de cierre generado " & ComandosDeJornadaFiscal.NumeroDeCierreZ)
            ProgressBar1.Visible = False
            Enabled = True
        End Sub

        Private Sub btn_por_numero_Click(ByVal sender As Object, ByVal e As EventArgs)
            If Impresora Is Nothing Then
                Mensaje("Impresora fuera de línea")
                Return
            End If

            ProgressBar1.Visible = True
            Enabled = False 'se desabilita el formulario
            Dim cierre1, cierre2 As Integer

            If Not Integer.TryParse(txt_z_1.Text, cierre1) Then
                Mensaje(String.Format("Error al convertir '{0}' en número", txt_z_1.Text))
                ProgressBar1.Visible = False
                Enabled = True
                Return
            End If

            If Not Integer.TryParse(txt_z_2.Text, cierre2) Then
                Mensaje(String.Format("Error al convertir '{0}' en número", txt_z_2.Text))
                ProgressBar1.Visible = False
                Enabled = True
                Return
            End If


            'Iniciamos el reporte
            Comandosdeauditoria.IniciarinformedecierresZporrangodecierresZ_Impreso(Impresora, cierre1.ToString(), cierre2.ToString())

            'Pedimos los datos
            Comandosdeauditoria.ObtenersiguientesdatosdeinformedecierresZ(Impresora)

            While Equals(Comandosdeauditoria.Indicadordedatospordescargar, "S")
                'Pedimos los datos mientras hayan disponibles
                Comandosdeauditoria.ObtenersiguientesdatosdeinformedecierresZ(Impresora)
            End While


            'Finalizaos el informe
            Comandosdeauditoria.FinalizarinformedecierresZ(Impresora)
            ProgressBar1.Visible = False
            Enabled = True
        End Sub

        Private Sub btn_por_fecha_Click(ByVal sender As Object, ByVal e As EventArgs)
            If Impresora Is Nothing Then
                Mensaje("Impresora fuera de línea")
                Return
            End If

            ProgressBar1.Visible = True
            Enabled = False 'se desabilita el formulario
            Dim fecha1, fecha2 As Date

            If Not Date.TryParse(DateTimePicker1.Text, fecha1) Then
                Mensaje(String.Format("Error al convertir '{0}' en número", DateTimePicker1.Text))
            End If

            If Not Date.TryParse(DateTimePicker2.Text, fecha2) Then
                Mensaje(String.Format("Error al convertir '{0}' en número", DateTimePicker2.Text))
            End If


            'Iniciamos el reporte
            Comandosdeauditoria.IniciarInformeCierreZporFecha_Impreso(Impresora, fecha1.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture).Replace("/", ""), fecha1.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture).Replace("/", ""))

            'Pedimos los datos
            Comandosdeauditoria.ObtenersiguientesdatosdeinformedecierresZ(Impresora)

            While Equals(Comandosdeauditoria.Indicadordedatospordescargar, "S")
                'Pedimos los datos mientras hayan disponibles
                Comandosdeauditoria.ObtenersiguientesdatosdeinformedecierresZ(Impresora)
            End While


            'Finalizamos el informe
            Comandosdeauditoria.FinalizarinformedecierresZ(Impresora)
            ProgressBar1.Visible = False
            Enabled = True
        End Sub

        Private Sub btn_libro_de_venta_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim ruta As String = Directory.GetCurrentDirectory() & "\Libro\testDllApp.exe"
            Process.Start(ruta)
        End Sub
    End Class
End Namespace
