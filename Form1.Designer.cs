﻿namespace pos_fiscal_CSharp
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.ProgressBar1 = new System.Windows.Forms.ProgressBar();
            this.GroupBox4 = new System.Windows.Forms.GroupBox();
            this.btn_por_fecha = new System.Windows.Forms.Button();
            this.btn_por_numero = new System.Windows.Forms.Button();
            this.DateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.DateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.txt_z_1 = new System.Windows.Forms.TextBox();
            this.txt_z_2 = new System.Windows.Forms.TextBox();
            this.GroupBox3 = new System.Windows.Forms.GroupBox();
            this.btn_cierre_z = new System.Windows.Forms.Button();
            this.btn_informe_x = new System.Windows.Forms.Button();
            this.GroupBox2 = new System.Windows.Forms.GroupBox();
            this.btn_nc_exoneracion = new System.Windows.Forms.Button();
            this.btn_nc_credito_fiscal = new System.Windows.Forms.Button();
            this.btn_nc_consumidor_final = new System.Windows.Forms.Button();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_fact_con_exoneracion_de_impuestos = new System.Windows.Forms.Button();
            this.btn_fact_credito_fiscal = new System.Windows.Forms.Button();
            this.cmd_fact_consumidor_final = new System.Windows.Forms.Button();
            this.btn_no_venta = new System.Windows.Forms.Button();
            this.btn_libro_de_venta = new System.Windows.Forms.Button();
            this.btn_reconectar = new System.Windows.Forms.Button();
            this.GroupBox4.SuspendLayout();
            this.GroupBox3.SuspendLayout();
            this.GroupBox2.SuspendLayout();
            this.GroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ProgressBar1
            // 
            this.ProgressBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ProgressBar1.Location = new System.Drawing.Point(0, 544);
            this.ProgressBar1.MarqueeAnimationSpeed = 25;
            this.ProgressBar1.Name = "ProgressBar1";
            this.ProgressBar1.Size = new System.Drawing.Size(392, 15);
            this.ProgressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.ProgressBar1.TabIndex = 15;
            // 
            // GroupBox4
            // 
            this.GroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBox4.Controls.Add(this.btn_por_fecha);
            this.GroupBox4.Controls.Add(this.btn_por_numero);
            this.GroupBox4.Controls.Add(this.DateTimePicker2);
            this.GroupBox4.Controls.Add(this.DateTimePicker1);
            this.GroupBox4.Controls.Add(this.txt_z_1);
            this.GroupBox4.Controls.Add(this.txt_z_2);
            this.GroupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox4.Location = new System.Drawing.Point(12, 441);
            this.GroupBox4.Name = "GroupBox4";
            this.GroupBox4.Size = new System.Drawing.Size(368, 89);
            this.GroupBox4.TabIndex = 14;
            this.GroupBox4.TabStop = false;
            this.GroupBox4.Text = "Auditoria";
            // 
            // btn_por_fecha
            // 
            this.btn_por_fecha.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_por_fecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.btn_por_fecha.Location = new System.Drawing.Point(257, 53);
            this.btn_por_fecha.Name = "btn_por_fecha";
            this.btn_por_fecha.Size = new System.Drawing.Size(105, 23);
            this.btn_por_fecha.TabIndex = 8;
            this.btn_por_fecha.Text = "Por fecha";
            this.btn_por_fecha.UseVisualStyleBackColor = true;
            this.btn_por_fecha.Click += new System.EventHandler(this.btn_por_fecha_Click);
            // 
            // btn_por_numero
            // 
            this.btn_por_numero.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_por_numero.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.btn_por_numero.Location = new System.Drawing.Point(257, 28);
            this.btn_por_numero.Name = "btn_por_numero";
            this.btn_por_numero.Size = new System.Drawing.Size(105, 23);
            this.btn_por_numero.TabIndex = 7;
            this.btn_por_numero.Text = "Por Número";
            this.btn_por_numero.UseVisualStyleBackColor = true;
            this.btn_por_numero.Click += new System.EventHandler(this.btn_por_numero_Click);
            // 
            // DateTimePicker2
            // 
            this.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DateTimePicker2.Location = new System.Drawing.Point(130, 55);
            this.DateTimePicker2.Name = "DateTimePicker2";
            this.DateTimePicker2.Size = new System.Drawing.Size(117, 20);
            this.DateTimePicker2.TabIndex = 6;
            // 
            // DateTimePicker1
            // 
            this.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DateTimePicker1.Location = new System.Drawing.Point(6, 55);
            this.DateTimePicker1.Name = "DateTimePicker1";
            this.DateTimePicker1.Size = new System.Drawing.Size(117, 20);
            this.DateTimePicker1.TabIndex = 5;
            // 
            // txt_z_1
            // 
            this.txt_z_1.Location = new System.Drawing.Point(6, 29);
            this.txt_z_1.Name = "txt_z_1";
            this.txt_z_1.Size = new System.Drawing.Size(117, 20);
            this.txt_z_1.TabIndex = 4;
            // 
            // txt_z_2
            // 
            this.txt_z_2.Location = new System.Drawing.Point(130, 29);
            this.txt_z_2.Name = "txt_z_2";
            this.txt_z_2.Size = new System.Drawing.Size(117, 20);
            this.txt_z_2.TabIndex = 2;
            // 
            // GroupBox3
            // 
            this.GroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBox3.Controls.Add(this.btn_cierre_z);
            this.GroupBox3.Controls.Add(this.btn_informe_x);
            this.GroupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox3.Location = new System.Drawing.Point(12, 330);
            this.GroupBox3.Name = "GroupBox3";
            this.GroupBox3.Size = new System.Drawing.Size(368, 105);
            this.GroupBox3.TabIndex = 13;
            this.GroupBox3.TabStop = false;
            this.GroupBox3.Text = "Reportes";
            // 
            // btn_cierre_z
            // 
            this.btn_cierre_z.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_cierre_z.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Italic);
            this.btn_cierre_z.Location = new System.Drawing.Point(6, 60);
            this.btn_cierre_z.Name = "btn_cierre_z";
            this.btn_cierre_z.Size = new System.Drawing.Size(356, 29);
            this.btn_cierre_z.TabIndex = 1;
            this.btn_cierre_z.Text = "Cierre Z";
            this.btn_cierre_z.UseVisualStyleBackColor = true;
            this.btn_cierre_z.Click += new System.EventHandler(this.btn_cierre_z_Click);
            // 
            // btn_informe_x
            // 
            this.btn_informe_x.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_informe_x.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Italic);
            this.btn_informe_x.Location = new System.Drawing.Point(6, 25);
            this.btn_informe_x.Name = "btn_informe_x";
            this.btn_informe_x.Size = new System.Drawing.Size(356, 29);
            this.btn_informe_x.TabIndex = 0;
            this.btn_informe_x.Text = "Informe X";
            this.btn_informe_x.UseVisualStyleBackColor = true;
            this.btn_informe_x.Click += new System.EventHandler(this.btn_informe_x_Click);
            // 
            // GroupBox2
            // 
            this.GroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBox2.Controls.Add(this.btn_nc_exoneracion);
            this.GroupBox2.Controls.Add(this.btn_nc_credito_fiscal);
            this.GroupBox2.Controls.Add(this.btn_nc_consumidor_final);
            this.GroupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox2.Location = new System.Drawing.Point(12, 186);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Size = new System.Drawing.Size(368, 138);
            this.GroupBox2.TabIndex = 12;
            this.GroupBox2.TabStop = false;
            this.GroupBox2.Text = "Notas de crédito";
            // 
            // btn_nc_exoneracion
            // 
            this.btn_nc_exoneracion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_nc_exoneracion.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Italic);
            this.btn_nc_exoneracion.Location = new System.Drawing.Point(6, 95);
            this.btn_nc_exoneracion.Name = "btn_nc_exoneracion";
            this.btn_nc_exoneracion.Size = new System.Drawing.Size(356, 29);
            this.btn_nc_exoneracion.TabIndex = 2;
            this.btn_nc_exoneracion.Text = "Nota de crédito con exoneración";
            this.btn_nc_exoneracion.UseVisualStyleBackColor = true;
            this.btn_nc_exoneracion.Click += new System.EventHandler(this.btn_nc_exoneracion_Click);
            // 
            // btn_nc_credito_fiscal
            // 
            this.btn_nc_credito_fiscal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_nc_credito_fiscal.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Italic);
            this.btn_nc_credito_fiscal.Location = new System.Drawing.Point(6, 60);
            this.btn_nc_credito_fiscal.Name = "btn_nc_credito_fiscal";
            this.btn_nc_credito_fiscal.Size = new System.Drawing.Size(356, 29);
            this.btn_nc_credito_fiscal.TabIndex = 1;
            this.btn_nc_credito_fiscal.Text = "Nota de crédito a crédito fiscal";
            this.btn_nc_credito_fiscal.UseVisualStyleBackColor = true;
            this.btn_nc_credito_fiscal.Click += new System.EventHandler(this.btn_nc_credito_fiscal_Click);
            // 
            // btn_nc_consumidor_final
            // 
            this.btn_nc_consumidor_final.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_nc_consumidor_final.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Italic);
            this.btn_nc_consumidor_final.Location = new System.Drawing.Point(6, 25);
            this.btn_nc_consumidor_final.Name = "btn_nc_consumidor_final";
            this.btn_nc_consumidor_final.Size = new System.Drawing.Size(356, 29);
            this.btn_nc_consumidor_final.TabIndex = 0;
            this.btn_nc_consumidor_final.Text = "Nota de crédito a consumidor final";
            this.btn_nc_consumidor_final.UseVisualStyleBackColor = true;
            this.btn_nc_consumidor_final.Click += new System.EventHandler(this.btn_nc_consumidor_final_Click);
            // 
            // GroupBox1
            // 
            this.GroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBox1.Controls.Add(this.btn_fact_con_exoneracion_de_impuestos);
            this.GroupBox1.Controls.Add(this.btn_fact_credito_fiscal);
            this.GroupBox1.Controls.Add(this.cmd_fact_consumidor_final);
            this.GroupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox1.Location = new System.Drawing.Point(12, 42);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(368, 138);
            this.GroupBox1.TabIndex = 11;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Facturación";
            // 
            // btn_fact_con_exoneracion_de_impuestos
            // 
            this.btn_fact_con_exoneracion_de_impuestos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_fact_con_exoneracion_de_impuestos.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_fact_con_exoneracion_de_impuestos.Location = new System.Drawing.Point(6, 95);
            this.btn_fact_con_exoneracion_de_impuestos.Name = "btn_fact_con_exoneracion_de_impuestos";
            this.btn_fact_con_exoneracion_de_impuestos.Size = new System.Drawing.Size(356, 29);
            this.btn_fact_con_exoneracion_de_impuestos.TabIndex = 2;
            this.btn_fact_con_exoneracion_de_impuestos.Text = "Factura con exoneración de impuestos";
            this.btn_fact_con_exoneracion_de_impuestos.UseVisualStyleBackColor = true;
            this.btn_fact_con_exoneracion_de_impuestos.Click += new System.EventHandler(this.btn_fact_con_exoneracion_de_impuestos_Click);
            // 
            // btn_fact_credito_fiscal
            // 
            this.btn_fact_credito_fiscal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_fact_credito_fiscal.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_fact_credito_fiscal.Location = new System.Drawing.Point(6, 60);
            this.btn_fact_credito_fiscal.Name = "btn_fact_credito_fiscal";
            this.btn_fact_credito_fiscal.Size = new System.Drawing.Size(356, 29);
            this.btn_fact_credito_fiscal.TabIndex = 1;
            this.btn_fact_credito_fiscal.Text = "Factura crédito fiscal";
            this.btn_fact_credito_fiscal.UseVisualStyleBackColor = true;
            this.btn_fact_credito_fiscal.Click += new System.EventHandler(this.btn_fact_credito_fiscal_Click);
            // 
            // cmd_fact_consumidor_final
            // 
            this.cmd_fact_consumidor_final.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmd_fact_consumidor_final.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd_fact_consumidor_final.Location = new System.Drawing.Point(6, 25);
            this.cmd_fact_consumidor_final.Name = "cmd_fact_consumidor_final";
            this.cmd_fact_consumidor_final.Size = new System.Drawing.Size(356, 29);
            this.cmd_fact_consumidor_final.TabIndex = 0;
            this.cmd_fact_consumidor_final.Text = "Factura consumidor final";
            this.cmd_fact_consumidor_final.UseVisualStyleBackColor = true;
            this.cmd_fact_consumidor_final.Click += new System.EventHandler(this.cmd_fact_consumidor_final_Click);
            // 
            // btn_no_venta
            // 
            this.btn_no_venta.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_no_venta.Location = new System.Drawing.Point(105, 12);
            this.btn_no_venta.Name = "btn_no_venta";
            this.btn_no_venta.Size = new System.Drawing.Size(87, 23);
            this.btn_no_venta.TabIndex = 10;
            this.btn_no_venta.Text = "No Venta";
            this.btn_no_venta.UseVisualStyleBackColor = true;
            this.btn_no_venta.Click += new System.EventHandler(this.btn_no_venta_Click);
            // 
            // btn_libro_de_venta
            // 
            this.btn_libro_de_venta.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_libro_de_venta.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_libro_de_venta.Location = new System.Drawing.Point(198, 12);
            this.btn_libro_de_venta.Name = "btn_libro_de_venta";
            this.btn_libro_de_venta.Size = new System.Drawing.Size(182, 23);
            this.btn_libro_de_venta.TabIndex = 9;
            this.btn_libro_de_venta.Text = "Libro de venta";
            this.btn_libro_de_venta.UseVisualStyleBackColor = true;
            this.btn_libro_de_venta.Click += new System.EventHandler(this.btn_libro_de_venta_Click);
            // 
            // btn_reconectar
            // 
            this.btn_reconectar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_reconectar.Location = new System.Drawing.Point(12, 12);
            this.btn_reconectar.Name = "btn_reconectar";
            this.btn_reconectar.Size = new System.Drawing.Size(87, 23);
            this.btn_reconectar.TabIndex = 8;
            this.btn_reconectar.Text = "Reconectar";
            this.btn_reconectar.UseVisualStyleBackColor = true;
            this.btn_reconectar.Click += new System.EventHandler(this.btn_reconectar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(392, 559);
            this.Controls.Add(this.ProgressBar1);
            this.Controls.Add(this.GroupBox4);
            this.Controls.Add(this.GroupBox3);
            this.Controls.Add(this.GroupBox2);
            this.Controls.Add(this.GroupBox1);
            this.Controls.Add(this.btn_no_venta);
            this.Controls.Add(this.btn_libro_de_venta);
            this.Controls.Add(this.btn_reconectar);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "POS Fiscal";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.GroupBox4.ResumeLayout(false);
            this.GroupBox4.PerformLayout();
            this.GroupBox3.ResumeLayout(false);
            this.GroupBox2.ResumeLayout(false);
            this.GroupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.ProgressBar ProgressBar1;
        internal System.Windows.Forms.GroupBox GroupBox4;
        internal System.Windows.Forms.Button btn_por_fecha;
        internal System.Windows.Forms.Button btn_por_numero;
        internal System.Windows.Forms.DateTimePicker DateTimePicker2;
        internal System.Windows.Forms.DateTimePicker DateTimePicker1;
        internal System.Windows.Forms.TextBox txt_z_1;
        internal System.Windows.Forms.TextBox txt_z_2;
        internal System.Windows.Forms.GroupBox GroupBox3;
        internal System.Windows.Forms.Button btn_cierre_z;
        internal System.Windows.Forms.Button btn_informe_x;
        internal System.Windows.Forms.GroupBox GroupBox2;
        internal System.Windows.Forms.Button btn_nc_exoneracion;
        internal System.Windows.Forms.Button btn_nc_credito_fiscal;
        internal System.Windows.Forms.Button btn_nc_consumidor_final;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Button btn_fact_con_exoneracion_de_impuestos;
        internal System.Windows.Forms.Button btn_fact_credito_fiscal;
        internal System.Windows.Forms.Button cmd_fact_consumidor_final;
        internal System.Windows.Forms.Button btn_no_venta;
        internal System.Windows.Forms.Button btn_libro_de_venta;
        internal System.Windows.Forms.Button btn_reconectar;
    }
}

